import { LitElement, html, css } from 'lit-element';

export class FirstComponent extends LitElement {

  static get properties() {
    return {
      prop: { type: String },
      name: { type: String }
    };
  }

  constructor() {
    super();
    this.name = 'Desconocido';
  }

  static get styles() {
    return css`
      :host {
        display: block;
        box-sizing: border-box;
        color: grey;
        font-size: 20px;
      }

      *, *:before, *:after {
        box-sizing: inherit;
      }

      .name {
        color: rgb(45, 204, 205);
      }
    `;
  }

  render() {
    return html`
    El name es: <span class="name">${this.name}<span>
    `;
  }

}

customElements.define('first-component', FirstComponent);