import { LitElement, html, css } from 'lit-element';

export class TwoLitCounter extends LitElement {

  static get properties() {
    return {
      count: { type: Number },
    };
  }

  constructor() {
    super();
    this.count = 0;
  }

  static get styles() {
    return css`
      :host {
        display: block;
        box-sizing: border-box;
      }

      *, *:before, *:after {
        box-sizing: inherit;
      }

      :host {
        display: flex;
        gap: 16px;
        flex-direction: column;
        width: fit-content;
        font-size: 20px;      
      }

      .title {
        font-weight: bold
      }
    `;
  }

  render() {
    return html`
      <div class="title"> Haz click </div>
      <div> Llevas ${this.count} clicks! </div>
      <button @click=${() => this.count += 1}>Suma al contador</button>
    `;
  }

}

customElements.define('lit-counter', TwoLitCounter);