# sft-lit-element-cap #

### What is this repository for? ###

* Practise lit-element
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* npm install
* npm run start

### Now you can practise the exercises!

You can use _first-component.js_ as a basic component structure to create your new components.

Remember to save new components in _app/components_ folder.