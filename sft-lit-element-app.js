import { LitElement, html, css } from 'lit-element';
import "./app/components/1-first-component.js"
import "./app/components/2-lit-counter.js"

export class SftLitElementApp extends LitElement {
	static get properties() {
		return {};
	}

	constructor() {
		super();
	}

	static get styles() {
		return css`
			:host {
				display: block;
				box-sizing: border-box;
				font-family: Arial, Helvetica, sans-serif;
			}

			*,
			*:before,
			*:after {
				box-sizing: inherit;
			}

			h1 {
				padding-top: 20px;
				margin-top: 20px;
				border-top: 2px solid #1973b8;
				font-size: 28px;
				color: #1973b8;
			}
		`;
	}

	render() {
		return html`
      <!-- Ejercicio 1:  -->
      <h1>Ejercicio 1: Hello name</h1>
      <p>Crear un componente que muestre un saludo y pueda modificar el nombre mediante un atributo.</p>
    
      <h2>Componente por defecto</h2>
      <first-component></first-component>

      <h2>Componente con name por atriuto</h2>
      <first-component name="Roger"></first-component>

      <!-- Ejercicio 2:  -->
			<h1>Ejercicio 2: Contador clicks</h1>
			
      <lit-counter></lit-counter>
		`;
	}
}

customElements.define('sft-lit-element-app', SftLitElementApp);
